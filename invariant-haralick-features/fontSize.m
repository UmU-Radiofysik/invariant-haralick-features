function size = fontSize(which, figureHandle, textWidthFactor)

    if nargin < 3
        textWidthFactor = 1.0;
    end

    position = figureHandle.Position;
    width = position(3) * textWidthFactor;
    if strcmpi(which, 'text')
        factor = 0.013;
    end
    if strcmpi(which, 'title')
        factor = 0.019;
    end
    if strcmpi(which, 'label')
        factor = 0.013;
    end
    if strcmpi(which, 'legend')
        factor = 0.013;
    end
    if strcmpi(which, 'ticklabel')
        factor = 0.010;
    end
    if strcmpi(which, 'number')
        factor = 0.024;
    end

    size = ceil(width * factor);
