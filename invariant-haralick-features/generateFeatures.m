function features = generateFeatures(GLCMs, featureNames)

    if nargin < 2
        featureNames = getFeatureNames();
    end

    for i = 1:length(GLCMs)
        GLCM = GLCMs{i};
        features(i) = GLCMFeaturesInvariant(GLCM, featureNames);
    end
