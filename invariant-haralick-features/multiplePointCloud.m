function pts = multiplePointCloud(images, neighborhood)

    pts = [];
    for i = 1:length(images)
        I = images{i};
        pt = pointCloud(I, neighborhood);
        pts = [pts; pt];
    end
