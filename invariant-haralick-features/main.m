
generate_lq_data = true;
generate_hq_data = true;
plot_example_glcms = true;
plot_convergence = true;
plot_hq_mse = true;
plot_optimal_lower_glcm_size = true;
plot_heatmaps = true;

library = 'kylberg';
class_names = {'blanket1', 'blanket2', 'canvas1', 'ceiling1', 'ceiling2', ...
               'cushion1', 'floor1', 'floor2', 'grass1', 'lentils1', ...
               'linseeds1', 'oatmeal1', 'pearlsugar1', 'rice1', 'rice2', ...
               'rug1', 'sand1', 'scarf1', 'scarf2', 'screen1', 'seat1', ...
               'seat2', 'sesameseeds1', 'stone1', 'stone2', 'stone3', ...
               'stoneslab1', 'wall1'};
DBPath = 'data';

GLCMSizes = [4:49, 50:2:256];
nPoints = round(logspace(log10(200), log10(100000), 21));
neighbourhood = true(3);
neighbourhood(2, 2) = false;
bounds = [0, 255, 0, 255];
nPointsIdeal = 100000000;

% Generate LQ data
if generate_lq_data
    % Compute for all texture classes
    GLCMReps = 300;
    storeSE = false;
    featureNames = getFeatureNames();
    for i = 1:length(class_names)
        class_name = class_names{i};
        images = readImages(library, class_name);

        DBID = sprintf('%s_%s_%d', library, class_name, GLCMReps);
        DBPath = 'data';

        [mse, bias2, variance, mse_std, idealGLCMs, idealFeatures] ...
            = analyseImageFeatures(images, GLCMSizes, nPoints, neighbourhood, ...
                                   bounds, nPointsIdeal, GLCMReps, featureNames, ...
                                   DBID, DBPath, storeSE);
    end
end

% Generate high-quality data from a single texture class
if generate_hq_data
    class_name = 'blanket1';  % For the type example plot
    images = readImages(library, class_name);
    GLCMReps = 500;
    storeSE = true;
    featureNames = {'energy', 'homogeneity'};
    DBID = sprintf('%s_%s_%d_HQ', library, class_name, GLCMReps);
    DBPath = 'data';

    [mse, bias2, variance, mse_std, idealGLCMs, idealFeatures] ...
        = analyseImageFeatures(images, GLCMSizes, nPoints, neighbourhood, ...
                               bounds, nPointsIdeal, GLCMReps, featureNames, ...
                               DBID, DBPath, storeSE);
end

% Plot examples of GLCMs
if plot_example_glcms
    GLCMReps = 300;
    delta = [0.05, 0.05];

    classNames = {'scarf1', 'oatmeal1', 'blanket1'};
    figureHandle = figure();
    figureHandle.Position = [10, 10, 1000, 650];
    for i = 1:3
        class_name = classNames{i};

        im = readImages(library, class_name);

        DBID = sprintf('%s_%s_%d', library, class_name, GLCMReps);
        filename = sprintf('%s/%s_ideal_GLCMs.mat', DBPath, DBID);
        DATA = load(filename);
        idealGLCMs = DATA.idealGLCMs;

        I = idealGLCMs{end};
        m = max(max(I));
        I(:, 1) = m;
        I(:, end) = m;
        I(1, :) = m;
        I(end, :) = m;

        modsubplot(2, 3, i, delta);
        imagesc(im{1});
        colormap(gray(256));
        axis('equal');
        axis('off');
        title(sprintf('\\texttt{%s}', class_name), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));

        modsubplot(2, 3, 3 + i, delta);
        imagesc(I);
        colormap(flipud(gray(256)));
        axis('equal');
        axis('off');
    end
end

% Plot convergence plot
if plot_convergence
    class_name = 'oatmeal1';
    GLCMReps = 300;
    featureNames = getFeatureNames();
    DBID = sprintf('%s_%s_%d', library, class_name, GLCMReps);
    filename = sprintf('%s/%s_MSE.mat', DBPath, DBID);
    DATA = load(filename);
    idealFeatures = DATA.idealFeatures;
    filename = sprintf('%s/%s_ideal_GLCMs.mat', DBPath, DBID);
    DATA = load(filename);
    idealGLCMs = DATA.idealGLCMs;

    plotConvergence(GLCMSizes, idealGLCMs, idealFeatures, featureNames);
end

% Plot high-quality MSE for a single texture class
if plot_hq_mse
    class_name = 'blanket1';
    images = readImages(library, class_name);
    GLCMReps = 500;
    DBID = sprintf('%s_%s_%d_HQ', library, class_name, GLCMReps);
    filename = sprintf('%s/%s_MSE.mat', DBPath, DBID);
    DATA = load(filename);
    mse = DATA.mse;
    mse_std = DATA.mse_std;
    se = DATA.se;

    plotOptima(GLCMSizes, nPoints, mse, mse_std, se, GLCMReps);
end

% Generate optimal and lower bound for the optimal GLCM size
if plot_optimal_lower_glcm_size
    GLCMReps = 300;
    [featureNames, C1, C2] = getFeatureNames();
    num_classes = length(class_names);
    for i = 1:num_classes
        class_name = class_names{i};
        DBID = sprintf('%s_%s_%d', library, class_name, GLCMReps);
        filename = sprintf('%s/%s_MSE.mat', DBPath, DBID);
        DATA = load(filename);
        mse = DATA.mse;
        mse_std = DATA.mse_std;
        OptGLCMSizes(i) = generateOptima(nPoints, mse, mse_std, GLCMSizes, GLCMReps, featureNames);
    end

    figureHandle = figure();
    figureHandle.Position = [10, 10, 1200, 750];
    delta = [0.05, 0.085];
    posMap = [1, 4, 5, 6, 7, 8, 9];
    for i = 1:length(C1)
        featureName = C1{i};

        modsubplot(3, 3, posMap(i), delta);
        V = [];
        for j = 1:num_classes
            values = [OptGLCMSizes(j).(featureName)];
            V = [V; values];
        end
        for j = 1:num_classes
            semilogx(nPoints, V(j, :), '.', 'Color', [0.7, 0.7, 0.7]);
            hold('on');
        end

        upper = prctile(V, 95, 1);
        lower = prctile(V, 5, 1);
        c = [0.9, 0.9, 0.9];
        jbfill([0.93*nPoints(1), nPoints, nPoints(end)*1.07], ...
               [upper(1), upper, upper(end)], ...
               [lower(1), lower, lower(end)], c, c, false);
        plot(nPoints, median(V, 1), 'k-');
        plot(nPoints, mean(V, 1), 'k:');
%         plot(nPoints, prctile(V, 5, 1), 'k--');
%         plot(nPoints, prctile(V, 95, 1), 'k--');
        xlim([0.9 * min(nPoints), max(nPoints) * 1.1]);
        ylim([0, 256]);
        set(gca, 'XTick', [100, 1000, 10000, 100000]);
        set(gca, 'TickLabelInterpreter', 'LaTeX');
        h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
        h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
        if posMap(i) >= 7
            xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
        end
        if posMap(i) == 1 || posMap(i) == 4 || posMap(i) == 7
            ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
        end
        title(featureNameMap(featureName, true), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    end

    figureHandle = figure();
    figureHandle.Position = [10, 10, 1200, 1000];
    delta = [0.05, 0.085];
    for i = 1:length(C2)
        featureName = C2{i};

        modsubplot(4, 3, i, delta);
        V = [];
        for j = 1:num_classes
            values = [OptGLCMSizes(j).(featureName)];
            V = [V; values];
        end
        for j = 1:num_classes
            semilogx(nPoints, V(j, :), '.', 'Color', [0.7, 0.7, 0.7]);
            hold('on');
        end

        c = [0.9, 0.9, 0.9];
        x = [0.9 * nPoints(1), nPoints, nPoints(end) * 1.1];
        upper = 256 * ones(1, length(nPoints));
        upper = [upper(1), upper, upper(end)];
        lower = prctile(V, 100, 1);
        lower = [lower(1), lower, lower(end)];
        jbfill([0.93 * nPoints(1), nPoints, nPoints(end) * 1.07], 0.99 * upper, lower, c, c, false);
        plot(x, lower, 'k-');
        ylim([0, max(V(:))]);
        xlim([0.9 * min(nPoints), max(nPoints) * 1.1]);
        ylim([0, 256]);

        set(gca, 'XTick', [100, 1000, 10000, 100000]);
        set(gca, 'TickLabelInterpreter', 'LaTeX');
        h = get(gca, 'XAxis'); h.FontSize = fontSize('ticklabel', figureHandle);
        h = get(gca, 'YAxis'); h.FontSize = fontSize('ticklabel', figureHandle);

        if i >= 10
            xlabel('Number of samples, $N$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
        end
        if i == 1 || i == 4 || i == 7 || i == 10
            ylabel('GLCM size, $n$', 'Interpreter', 'LaTeX', 'FontSize', fontSize('label', figureHandle));
        end
        title(featureNameMap(featureName), 'Interpreter', 'LaTeX', 'FontSize', fontSize('title', figureHandle));
    end
end

% Plot the T-test p-value heatmaps
if plot_heatmaps
    GLCMReps = 300;
    num_classes = length(class_names);
    allIdealFeatures = [];
    allMSE = [];
    featureNames = getFeatureNames();
    for i = 1:length(featureNames)
        featureName = featureNames{i};

        allIdealFeatures.(featureName) = [];
        allMSE.(featureName) = 0;
    end
    for i = 1:num_classes
        class_name = class_names{i};

        DBID = sprintf('%s_%s_%d', library, class_name, GLCMReps);
        filename = sprintf('%s/%s_MSE.mat', DBPath, DBID);
        DATA = load(filename);
        mse = DATA.mse;
        idealFeatures = DATA.idealFeatures;

        for j = 1:length(featureNames)
            featureName = featureNames{j};

            f = [idealFeatures.(featureName)];
            allIdealFeatures.(featureName) = [allIdealFeatures.(featureName), f(end)];
        end
        for j = 1:length(featureNames)
            MSE = [];
            for k = 1:length(nPoints)
                featureName = featureNames{j};

                m = mse{k}.(featureName);

                MSE = [MSE, m(:)];
            end
            allMSE.(featureName) = allMSE.(featureName) + MSE / num_classes;
        end
    end
    for j = 1:length(featureNames)
        featureName = featureNames{j};

        f = allIdealFeatures.(featureName);
        pairwiseMeanDistance.(featureName) = 2 * var(f);
    end

    plotFCurves(allMSE, pairwiseMeanDistance, featureNames, nPoints);
end
