function [mse, bias2, variance, mse_std, se] = computeMSE(pts, ns, npoints, reps, fs_ideal, neighbourhood, bounds, featureNames)

    if nargin < 8
        featureNames = getFeatureNames();
    end

    for i = 1:length(ns)
        n = ns(i);
        GLCMs = GLCMbin(pts, npoints, reps, n, neighbourhood, bounds);

        GLCMMat = cat(3, GLCMs{:});
        f = GLCMFeaturesInvariant(GLCMMat, featureNames);
        for ft = 1:length(featureNames)
            Xn = [f(:).(featureNames{ft})];
            X0 = fs_ideal.(featureNames{ft});
            [ms, v, b2, msv] = computeMSEStatistics(Xn, X0, 100);
            variance.(featureNames{ft})(i) = v;
            bias2.(featureNames{ft})(i) = b2;
            mse.(featureNames{ft})(i) = ms;
            mse_std.(featureNames{ft})(i) = sqrt(msv);
            se.(featureNames{ft})(i, :) = (Xn - X0).^2;
        end
    end
